fetch('http://localhost:3000/metrics/users/000000-1/pages/http%3A%2F%2Fwww.example.com%2F')
    .then(
        function(response) {
            if (response.status !== 200) {
                console.log('Looks like there was a problem. Status Code: ' +
                    response.status);
                return;
            }

            // Examine the text in the response
            response.json().then(function(data) {
                console.log(data);


                // see documentation here https://www.patrick-wied.at/static/heatmapjs/docs.html
                var heatmap = h337.create({
                    container: document.querySelector(".heatmap")
                });

                heatmap.setData({
                    max: 10,
                    data: data.map(m => {return { x: m.x, y: m.y, value: m.total }})
                });



            });
        }
    )
    .catch(function(err) {
        console.log('Fetch Error :-S', err);
    });