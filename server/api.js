const restify = require('restify');

module.exports = function() {

    var api = restify.createServer();

    api.use(restify.CORS()); // http://enable-cors.org/
    api.use(restify.bodyParser());
    api.use(restify.queryParser());

    // load API resources
    api.get('/health', require('./resources/health/get.health.js'));

    api.get('/metrics/users/:user_id/pages/:url', require('./resources/metrics/get.metrics.js'));
    api.post('/metrics', require('./resources/metrics/post.metrics.js'));

    return api;

};