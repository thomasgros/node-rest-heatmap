const chai = require('chai');
const expect = chai.expect;
const request = require('supertest');

const api = require('../api')();

before(function listen(done) {
    api.listen(0, function listening() {
        console.log('test api listening on http://localhost:' + api.address().port);
        done();
    });
});

after(function (done) {
    api.close( function() {
        done();
    });
});

describe('api', function() {
    describe('GET /health', function() {
        it('should return {"status":"ok"}', function(done) {
            request(api)
                .get('/health')
                .expect(200)
                .expect('Content-Type', 'application/json')
                .expect({"status":"ok"}) // supertest sait tester le body directement
                // supertest nous permet aussi de déclencher nos propres tests via un callback
                // .expect(function(res) {
                //     expect(res.body).to.deep.equal({"status":"ok"});
                // })
                .end(done);
        });

        it('should return <health><state>ok</state></health> if Accept: application/xml', function(done) {
            request(api)
                .get('/health')
                .set('Accept', 'application/xml')
                .expect(200)
                .expect('Content-Type', 'application/xml')
                .expect('<health><state>ok</state></health>')
                .end(done);
        });

        it('should return 406 if Accept is not json nor xml', function(done) {
            request(api)
                .get('/health')
                .set('Accept', 'application/foo')
                .expect(406)
                .end(done);
        });

        it('should return {"status":"ok"} if no Accept header is present', function(done) {
            request(api)
                .get('/health')
                .expect(200)
                .expect('Content-Type', 'application/json')
                .expect({"status":"ok"}) // supertest sait tester le body directement
                .end(done);
        })
    })
});