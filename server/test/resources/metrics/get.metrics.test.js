const fs = require('fs');
const path = require('path');
const chai = require('chai');
const expect = chai.expect;
const request = require('supertest');

const MongoClient = require('mongodb').MongoClient;
// Connection URL
const url = 'mongodb://localhost:27017/metricsapi';

const api = require('../../../api')();

let database;

function sortMetricsByXYTotal(m1, m2) {
    if(m1.x < m2.x) { return -1;}
    if(m1.x > m2.x) { return 1;}

    if(m1.y < m2.y) { return -1;}
    if(m1.y > m2.y) { return 1;}

    if(m1.total < m2.total) { return -1;}
    if(m1.total > m2.total) { return 1;}
}

function metricsAreTheSame(arr1, arr2) {
    if(arr1 === null && arr2 === null) {return true }
    if(arr1 === null || arr2 === null) { return false }
    if (arr1.length != arr2.length) { return false }

    arr1.sort(sortMetricsByXYTotal);
    arr2.sort(sortMetricsByXYTotal);

    for (let i = 0; i < arr1.length; i++) {
        if(arr1[i].x !== arr2[i].x
        || arr1[i].y !== arr2[i].y
        || arr1[i].total !== arr2[i].total) {
            return false;
        }
    }

    return true;
}

describe('metricsapi', function() {

    before(function listen(done) {
        MongoClient.connect(url, function(err, db) {
            expect(err).to.be.null;

            database = db;

            api.listen(0, function listening() {
                done();
            });
        });

    });

    after(function (done) {
        api.close( function() {
            database.close();
            done();
        });
    });

    beforeEach(function(done) {
        const collection = database.collection('metrics');
        collection.drop(function(err, reply) {
            fs.readFile(path.resolve(__dirname, 'metrics.json'), 'utf8', function (err, data) {
                expect(err).to.be.null;

                database.collection('metrics').insertMany(JSON.parse(data), function (err, doc) {
                    expect(err).to.be.null;
                    done();
                });
            });
        });
    });

    describe('GET /metrics/users/000000-1/pages/http%3A%2F%2Fwww.example.com%2F', function() {
        it('should get aggregated data for user 000000-1 and page http://www.example.com/', function(done) {

            fs.readFile(path.resolve(__dirname, 'metrics-aggregation-results.json'), 'utf8', function (err, data) {
                expect(err).to.be.null;

                const expectedResults = JSON.parse(data);

                request(api)
                    .get('/metrics/users/000000-1/pages/http%3A%2F%2Fwww.example.com%2F')
                    .expect(200)
                    .expect('Content-Type', 'application/json')
                    .expect(function(res) {
                        expect(metricsAreTheSame(expectedResults, res.body)).to.be.true;

                        // expect(expectedResults).to.deep.equal(res.body); // in fact does not work if array are not sorted :-(
                        // expect(expectedResults).to.have.same.deep.members(res.body) // should work but seems to hang indefinetly :-(
                    })
                    .end(done);
            });
        });
    })
});