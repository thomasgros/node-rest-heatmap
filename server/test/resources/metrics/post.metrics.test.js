const chai = require('chai');
const expect = chai.expect;
const request = require('supertest');

const MongoClient = require('mongodb').MongoClient;
// Connection URL
const url = 'mongodb://localhost:27017/metricsapi';

const api = require('../../../api')();

let database;

describe('metricsapi', function() {

    before(function listen(done) {
        MongoClient.connect(url, function(err, db) {
            expect(err).to.be.null;
            console.log("Connected successfully to server");
            database = db;

            api.listen(0, function listening() {
                console.log('test api listening on http://localhost:' + api.address().port);
                done();
            });
        });

    });

    after(function (done) {
        api.close( function() {
            database.close();
            done();
        });
    });

    beforeEach(function(done) {
        const collection = database.collection('metrics');
        collection.drop(function(err, reply) {
            done();
        });
    });

    describe('POST /metrics', function() {
        it('should persist one metric inside the database', function(done) {
            const metricsToSend = [{
                page: "http://www.example.com",
                user: "azertyuiop-123456789",
                x: 50,
                y:100,
                instant: Date.now()
            }];
            request(api)
                .post('/metrics')
                .set('Content-Type', 'application/json')
                .send(metricsToSend)
                .expect(201) // 201 => CREATED
                .end(function(err, res) {
                    if (err) return done(err);

                    var collection = database.collection('metrics');
                    collection.find({}).toArray(function(err, docs) {
                        expect(err).to.be.null;
                        expect(docs.length).to.equal(1);

                        const docFromDb = docs[0];
                        delete docFromDb._id; // removing _id prop
                        expect(docFromDb).to.deep.equal(metricsToSend[0]);
                        return done(err, res);
                    });
                 });
        });
    })
});