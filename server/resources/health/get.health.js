/**
 * GET /health
 *
 * Indique si l'API est en bonne santé ou non.
 *
 * Accepte les représentations application/json
 * {'status' : 'ok'}
 *
 * et application/xml
 * <health><state>ok</state></health>
 */

module.exports = function respond(req, res, next) {
    if(req.accepts('application/json')) { // undefined accepts all
        const health = {
            'status' : 'ok'
        };
        res.send(health);
        return next();
    } else if(req.accepts('application/xml')) {
        res.writeHead(200, {
            'Content-Type': 'application/xml'
        });
        res.write('<health><state>ok</state></health>');
        return res.end();
    } else {
        return next(new restify.NotAcceptableError ("Content-Type not accepted, use application/json or application/xml"));
    }
};