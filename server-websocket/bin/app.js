var app = require('http').createServer();
var io = require('socket.io')(app);
var fs = require('fs');

const subscribedClientsPerContext = {}; // new Map

const metricsDb = require('../db/metricsDb');

app.listen(80);

io.on('connection', function (socket) {

    socket.on('send-metrics',  function (data) {
        //TODO verify data is valid
        //console.log('received', data);
        metricsDb.insertMetrics(data, function(err, result){
            if(err) { return console.log(err); }
           // console.log("successfully inserted", result);
            console.log(data);
            const context = { user_id: data[0].user, url: data[0].page };
            console.log("context", context);
            const clients = subscribedClientsPerContext[JSON.stringify(context)];
            console.log("JSON.stringify(context)", JSON.stringify(context));
            console.log("subscribedClientsPerContext", subscribedClientsPerContext);
            console.log("clients", clients);
            if(! clients) { return }

            // TODO optimiser pour les clients ayant les meme timestamp
            clients.forEach(c => {
                metricsDb.getMetrics(Object.assign({}, context, { from: c.lastSendMetricsTimestamp }), function(err, data) {
                    if(err) { return console.log(err); }

                    // le Date.now devrait être calculé par rapport à l'aggregation
                    c.lastSendMetricsTimestamp = Date.now();
                    c.socket.emit('update-metrics', data);
                });
            });
        });
    });

    /**
     * context: { user_id, url }
     */
    socket.on('get-all-metrics', function(context) {
        // TODO check context is valid

        metricsDb.getMetrics(context, function(err, data) {
            if(err) { return console.log(err); }

            if(! subscribedClientsPerContext[JSON.stringify(context)]) {
                subscribedClientsPerContext[JSON.stringify(context)] = [];
            }

            // le Date.now devrait être calculé par rapport à l'aggregation
            subscribedClientsPerContext[JSON.stringify(context)].push({
                socket,
                lastSendMetricsTimestamp: Date.now()
            });

            socket.emit('update-metrics', data);
        });
    })

});

// TODO gestion des deconnexion
// TODO emit si erreur (BDD, etc..) pour notifier le client