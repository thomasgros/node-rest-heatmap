cache = [
    { x: 1, y: 2, total: 1},
    { x: 2, y: 3, total: 2},
    { x: 3, y: 4, total: 3},
    { x: 4, y: 5, total: 4},
    { x: 5, y: 6, total: 5},

];

const newData = [
    { x: 1, y: 1, total: 3},
    { x: 2, y: 3, total: 1},
    { x: 3, y: 4, total: 5},
    { x: 4, y: 4, total: 2},
    { x: 5, y: 6, total: 3},
    { x: 8, y: 8, total: 8}

];


cache.forEach(d => {

    let i = newData.length;
    while (i--) {
        if(newData[i].x === d.x && newData[i].y === d.y) {
            d.total += newData[i].total;
            newData.splice(i,1); // removes from newData
            break;
        }

    }
});

cache.push(...newData); // plain loop if no es6

console.log(cache);