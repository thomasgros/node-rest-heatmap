const DEFAULT_CONTEXT = {user_id: "user-123456789", url: "page-to-spy.html" };

const request = indexedDB.open("metrics");
let db;

request.onupgradeneeded = function() {
    // The database did not previously exist, so create object stores and indexes.
    var db = request.result;
    var store = db.createObjectStore("metricsByContext");

    store.put([], [DEFAULT_CONTEXT.user_id, DEFAULT_CONTEXT.url]);

    // store.put(metricsCache, JSON.Stringify({user, url}));
    // store.get(JSON.Stringify({user, url})) => metricscache

    // store.put(metricsCache, [user, url]);
    // store.get([user, url]) => metricsCache

    // store.put({ user, url, metricsCache}); // auto generated key or keypath: "user, url"
    // store.get("user, url") => { user, url, metricsCache}

    // sinon il est possible de fetch by index
    // fetch by index ?
};

request.onerror = function(e) {
    console.log("Can't open database", e)
};

request.onsuccess = function() {
    db = request.result;

    listenAndDraw();
};



function listenAndDraw() {
    const socket = io('http://localhost');

    let canvas = $("<canvas>")
        .attr("width", 400)
        .attr("height", 400);

    $("body").prepend(canvas);

    let timeStampSinceLastResult;

    socket.on('update-metrics', (data) => {
        timeStampSinceLastResult = Date.now(); // TODO perte de données potentielle ici, aggréger le latestTimeStamp côté serveur

        updateMetricsCacheWithNewData(data, function(err, data) {
            updateMetricsRepresentation();
        });

    });

    socket.emit('get-all-metrics', DEFAULT_CONTEXT);

    function updateMetricsCacheWithNewData(newMetrics, cb) {

        var tx = db.transaction("metricsByContext", "readwrite");
        var store = tx.objectStore("metricsByContext");

        const request = store.get([DEFAULT_CONTEXT.user_id, DEFAULT_CONTEXT.url]);

        request.onsuccess = function() {
            if(! request.result) { console.log("Can't get metricsCache"); return }
            const metricsCache = request.result;

            newMetrics.forEach(d => {

                let dataFoundInCache = metricsCache.find(cd => {
                    return (cd.x === d.x) && (cd.y === d.y)
                });

                if(dataFoundInCache) {
                    dataFoundInCache.total += d.total;
                } else {
                    metricsCache.push(d);
                }
            });

            const putRequest = store.put(metricsCache, [DEFAULT_CONTEXT.user_id, DEFAULT_CONTEXT.url]);
            putRequest.onerror = function() { console.log("Can't update metrics for", DEFAULT_CONTEXT)}

        };
        request.onerror = function(e) {
          console.log("Can't get metricsCache for key", [DEFAULT_CONTEXT.user_id, DEFAULT_CONTEXT.url], e);
        };
        tx.onabort = function(e) {
            console.log("transaction aborted", e);
            const err = new Error("transaction aborted");
            err.event = e;
            cb(err, null);
        };
        tx.onerror = function(e) {
            console.log("transaction error", e);
            const err = new Error("transaction error");
            err.event = e;
            cb(err, null);
        };
        tx.oncomplete = function(e) { // All requests have succeeded and the transaction has committed.
            console.log("transaction complete", e);
            cb(null, "transaction complete");
        };
    }

    function updateMetricsRepresentation() {

        var tx = db.transaction("metricsByContext", "readonly");
        var store = tx.objectStore("metricsByContext");
        const request = store.get([DEFAULT_CONTEXT.user_id, DEFAULT_CONTEXT.url]);

        request.onsuccess = function() {
            if(! request.result) { console.log("Can't get metricsCache"); return }
            const metricsCache = request.result;

            let ctx = canvas[0].getContext('2d');
            const imageData = ctx.createImageData(400, 400);
            const pixels = imageData.data;

            for (let i = 0; i < metricsCache.length; i++) {
                const metric = metricsCache[i];

                if (metric.x > 400 || metric.y > 400) {
                    continue;
                }

                imageData.data[4 * (metric.y * imageData.width + metric.x)] = 255;
                imageData.data[4 * (metric.y * imageData.width + metric.x) + 3] = 255;
            }

            ctx.putImageData(imageData, 0, 0);
        }
    }
}