self.addEventListener('install', function(event) {
    event.waitUntil(
        // fetchStuffAndInitDatabases()
        caches.open('assets').then(function(cache) {
            return cache.addAll([
                'img/offline-first-diagram.png',
                // '/styles/all.css',
                // '/styles/imgs/bg.png',
                // '/scripts/all.js'
            ]);
        }).then((cache) => {
            // console.log(cache);
            console.log("assets cache initialized");
        })
    );
});

let metricsNotSent = [];

self.addEventListener('fetch', function(event) {
    event.respondWith(

        caches.match(event.request).then(function(cachedResponse) {
            // if(cachedResponse) {
            //     console.log('#### found in cache ', event.request)
            // } else {
            //     console.log('#### not found in cache', event.request);
            // }

            if(/metrics/.test(event.request.url)) {
                // console.log("will try to send metrics", event.request);

                const requestClone = event.request.clone();

                return fetch(event.request).then(response => {
                    // console.log("did send metrics");
                    return response;
                }).catch(err => {
                    return requestClone.json()
                        .then(data => {
                            metricsNotSent = metricsNotSent.concat(data);
                            // console.log(metricsNotSent);
                        })
                        .then(() => {
                            // console.log("failed to send metrics");
                            return new Response('not working')
                        });
                })
            } else {
                // console.log("did not match regexp");
                return cachedResponse || fetch(event.request);
            }
        })
    );
});

setInterval(function() {
    console.log('metricsNotSent', metricsNotSent);

    console.log("entering setInterval", metricsNotSent.length);
    if(metricsNotSent.length == 0) { return }

    const metricsNotSentCache = metricsNotSent.splice(0);

    const request = new Request("http://localhost:3000/metrics", {
        method: 'POST',
        headers: {'Content-Type':'application/json'},
        body: JSON.stringify(metricsNotSentCache),
        mode: 'cors'
    });

    fetch(request).then(() => {
        console.log("did send metricsNotSent");
    }).catch(() => {
        console.log("did not metricsNotSent");
        metricsNotSent = metricsNotSent.concat(metricsNotSentCache);
    });

}, 2000);